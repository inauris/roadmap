# Helpfull links

- [Как учиться. Janki-метод](https://youtu.be/_dbSRVa1Lxw)
- [Git](https://www.youtube.com/playlist?list=PLDyvV36pndZFHXjXuwA_NywNrVQO0aQqb)
- [Web-приложение на низком уровне: сокеты и роутинг](https://www.youtube.com/watch?v=4haMUvUxUJI)
- [Асинхронность](https://youtube.com/playlist?list=PLlWXhlUMyooawilqK4lPXRvxtbYiw34S8)
- [Dunder methods](https://habr.com/ru/post/186608/)


## Authors:
- [Олег Молчанов](https://www.youtube.com/channel/UCD5_waDcGBhof9xuA1qovTQ)
- [Илья Кан](https://www.youtube.com/channel/UCKPbiit_j8ycmkutM0wVzdA)
